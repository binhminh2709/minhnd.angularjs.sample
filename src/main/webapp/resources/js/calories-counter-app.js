/**
 * Created by Administrator on 10/8/2015.
 */


angular.module('caloriesCounterApp', ['editableTableWidgets' , 'frontendServices', 'spring-security-csrf-token-interceptor'])
    .filter('excludeDeleted', function(){
        return function(input) {
            return _.filter(input, function (item) {
                return item.deleted == undefined || !item.deleted;
            });
        }
    })

    .controller('CaloriesTrackerCtrl', ['$scope' , 'MealService', 'UserService', '$timeout',
        function ($scope, MealService, UserService, $timeout) {

        }
    ])
