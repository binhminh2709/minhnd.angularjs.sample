/**
 * Created by Administrator on 10/9/2015.
 */
/** create module loginApp*/
angular.module('loginApp', ['common', 'spring-security-csrf-token-interceptor', 'editableTableWidgets']).
    controller('LoginCtrl', ['$scope','$http', function($scope, $http){
        $scope.onLogin = function() {
            console.log('Attempting login with username ' + $scope.vm.username + ' and password ' + $scope.vm.password);
            $scope.vm.submitted = true;

        }
    }])