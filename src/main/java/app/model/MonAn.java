package app.model;

import javax.persistence.*;

/**
 * Created by Administrator on 2/25/2016.
 */
@Entity
@Table(name = "mon_an")
public class MonAn extends AbstractEntity {

    private String tenMon;
    private String image;
    private Integer gia;

    public String getTenMon() {
        return tenMon;
    }

    public void setTenMon(String tenMon) {
        this.tenMon = tenMon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getGia() {
        return gia;
    }

    public void setGia(Integer gia) {
        this.gia = gia;
    }


}
