package app.model;

import java.util.List;

/**
 * Created by Administrator on 10/3/2015.
 */
public class SearchResult<T> {
    private long resultsCount;
    private List<T> result;

    public SearchResult() {
    }

    public SearchResult(long resultsCount, List<T> result) {
        this.resultsCount = resultsCount;
        this.result = result;
    }

    public long getResultsCount() {
        return resultsCount;
    }


    public List<T> getResult() {
        return result;
    }
}
