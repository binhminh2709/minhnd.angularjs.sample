package app.dao;

import app.model.MonAn;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Administrator on 2/26/2016.
 */
@Repository
public class MonAnRepository {

    private static final Logger LOGGER = Logger.getLogger(MonAnRepository.class);

    @PersistenceContext
    EntityManager em;

    public List<MonAn> findMealsByDateTime() {


        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<MonAn> searchQuery = cb.createQuery(MonAn.class);

        Root<MonAn> searchRoot = searchQuery.from(MonAn.class);
        searchQuery.select(searchRoot);
//        searchQuery.where(getCommonWhereCondition(cb, username, searchRoot, fromDate, toDate, fromTime, toTime));

//        List<Order> orderList = new ArrayList();
//        orderList.add(cb.desc(searchRoot.get("date")));
//        orderList.add(cb.asc(searchRoot.get("time")));
//        searchQuery.orderBy(orderList);

        TypedQuery<MonAn> filterQuery = em.createQuery(searchQuery);
//                .setFirstResult((pageNumber - 1) * 10)
//                .setMaxResults(10);

        return filterQuery.getResultList();
    }
}
