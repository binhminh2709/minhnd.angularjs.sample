package app.dao;

import app.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Administrator on 10/3/2015.
 */
@Repository
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public User findUserByUsername(String username) {
        List<User> users = em.createNamedQuery(User.FIND_BY_USERNAME, User.class).setParameter("username", username).getResultList();
        return users.size() == 1 ? users.get(0) : null;
    }

    public Long findTodaysCaloriesForUser(String username) {
        return (Long) em.createNamedQuery(User.COUNT_TODAYS_CALORIES).setParameter("username", username).getSingleResult();
    }

    public void save(User user) {
        em.merge(user);
    }

    public boolean isUsernameAvailable(String username) {

        List<User> users = em.createNamedQuery(User.FIND_BY_USERNAME, User.class).setParameter("username", username).getResultList();

        return users.isEmpty();
    }
}
