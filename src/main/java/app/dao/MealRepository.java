package app.dao;

import app.model.Meal;
import app.model.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Administrator on 10/3/2015.
 */

@Repository
public class MealRepository {

    private static final Logger LOGGER = Logger.getLogger(MealRepository.class);

    @PersistenceContext
    EntityManager em;

    public Long countMealsByDateTime(String username, Date fromDate, Date toDate, Time fromTime, Time toTime) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Meal> countRoot = criteriaQuery.from(Meal.class);

        criteriaQuery.select((criteriaBuilder.count(countRoot)));
        criteriaQuery.where(getCommonWhereCondition(criteriaBuilder, username, countRoot, fromDate, toDate, fromTime, toTime));

        Long resultsCount = em.createQuery(criteriaQuery).getSingleResult();

        LOGGER.info("Found " + resultsCount + " results.");

        return resultsCount;
    }

    public List<Meal> findMealsByDateTime(String username, Date fromDate, Date toDate, Time fromTime, Time toTime, int pageNumber) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<Meal> searchQuery = cb.createQuery(Meal.class);

        Root<Meal> searchRoot = searchQuery.from(Meal.class);
        searchQuery.select(searchRoot);
        searchQuery.where(getCommonWhereCondition(cb, username, searchRoot, fromDate, toDate, fromTime, toTime));

        List<Order> orderList = new ArrayList();
        orderList.add(cb.desc(searchRoot.get("date")));
        orderList.add(cb.asc(searchRoot.get("time")));
        searchQuery.orderBy(orderList);

        TypedQuery<Meal> filterQuery = em.createQuery(searchQuery)
                .setFirstResult((pageNumber - 1) * 10)
                .setMaxResults(10);

        return filterQuery.getResultList();
    }

    public void delete(Long deletedMealId) {
        Meal delete = em.find(Meal.class, deletedMealId);
        em.remove(delete);
    }

    public Meal findMealById(Long id) {
        return em.find(Meal.class, id);
    }

    public Meal save(Meal meal) {
        return em.merge(meal);
    }

    private Predicate[] getCommonWhereCondition(CriteriaBuilder cb, String username, Root<Meal> searchRoot, Date fromDate, Date toDate, Time fromTime, Time toTime) {
        List<Predicate> predicates = new ArrayList<>();
        Join<Meal, User> user = searchRoot.join("user");

        predicates.add(cb.equal(user.<String>get("username"), username));
        predicates.add(cb.greaterThanOrEqualTo(searchRoot.<Date>get("date"), fromDate));

        if (toDate != null) {
            predicates.add(cb.lessThanOrEqualTo(searchRoot.<Date>get("date"), toDate));
        }

        if (fromTime != null) {
            predicates.add(cb.greaterThanOrEqualTo(searchRoot.<Date>get("time"), fromTime));
        }

        if (toTime != null) {
            predicates.add(cb.lessThanOrEqualTo(searchRoot.<Date>get("time"), toTime));
        }

        return predicates.toArray(new Predicate[]{});
    }
}
