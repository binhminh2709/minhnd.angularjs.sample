package app.services;

import app.dao.MealRepository;
import app.dao.UserRepository;
import app.dto.MealDTO;
import app.model.Meal;
import app.model.SearchResult;
import app.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.Assert.notNull;

/**
 * Created by Administrator on 10/3/2015.
 * Business service for Meal-related operations.
 */

@Service
public class MealService {

    private static final Logger LOGGER = Logger.getLogger(MealService.class);

    @Autowired
    MealRepository mealRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional(readOnly = true)
    public SearchResult<Meal> findMeals(String username, Date fromDate, Date toDate, Time fromTime, Time toTime, int pageNumber) {
        if (fromDate == null || toDate == null) {
            throw new IllegalArgumentException("Both the from and to date are needed.");
        }

        if (fromDate.after(toDate)) {
            throw new IllegalArgumentException("From date cannot be after to date.");
        }

        if (fromDate.equals(toDate) && fromTime != null && toTime != null && fromTime.after(toTime)) {
            throw new IllegalArgumentException("On searches on the same day, from time cannot be after to time.");
        }

        Long resultsCount = mealRepository.countMealsByDateTime(username, fromDate, toDate, fromTime, toTime);

        List<Meal> meals = mealRepository.findMealsByDateTime(username, fromDate, toDate, fromTime, toTime, pageNumber);

        return new SearchResult<>(resultsCount, meals);
    }

    @Transactional
    public void deleteMeals(List<Long> deletedMealIds) {
        Assert.notNull(deletedMealIds, "deletedMealsId is mandatory");
        deletedMealIds.stream().forEach((deletedMealId) -> mealRepository.delete(deletedMealId));
    }


    @Transactional
    public Meal saveMeal(String username, Long id, Date date, Time time, String description, Long calories) {
        ValidationUtils.assertNotBlank(username, "username cannot be blank");
        notNull(date, "date is mandatory");
        notNull(time, "time is mandatory");
        notNull(description, "description is mandatory");
        notNull(calories, "calories is mandatory");

        Meal meal = null;

        if (id != null) {
            meal = mealRepository.findMealById(id);

            meal.setDate(date);
            meal.setTime(time);
            meal.setDescription(description);
            meal.setCalories(calories);
        } else {
            User user = userRepository.findUserByUsername(username);
            if (user != null) {
                meal = mealRepository.save(new Meal(user, date, time, description, calories));
                LOGGER.warn("A meal was attempted to be saved for a non-existing user: " + username);
            }
        }
        return meal;
    }

    @Transactional
    public List<Meal> saveMeals(String username, List<MealDTO> meals) {
        return meals.stream()
                .map((meal) -> saveMeal(
                        username,
                        meal.getId(),
                        meal.getDate(),
                        meal.getTime(),
                        meal.getDescription(),
                        meal.getCalories()))
                .collect(Collectors.toList());
    }

}
