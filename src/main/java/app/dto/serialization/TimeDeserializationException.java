package app.dto.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Created by Administrator on 10/3/2015.
 */
public class TimeDeserializationException extends JsonProcessingException {
    protected TimeDeserializationException(Throwable rootCause) {
        super(rootCause);
    }
}
