package app.dto;

import java.util.List;

/**
 * Created by Administrator on 10/3/2015.
 * JSON serializable DTO containing data concerning a meal search request.
 */
public class MealsDTO {

    private long currentPage;
    private long totalPages;
    List<MealDTO> meals;

    public MealsDTO() {
    }

    public MealsDTO(long currentPage, long totalPages, List<MealDTO> meals) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.meals = meals;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public List<MealDTO> getMeals() {
        return meals;
    }

    public void setMeals(List<MealDTO> meals) {
        this.meals = meals;
    }
}
