package app;

import app.model.Meal;
import config.root.TestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Administrator on 2/26/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "/hibernate.cfg.xml" })
@ActiveProfiles("test")
@ContextConfiguration(classes = {
        TestConfiguration.class
//        , RootContextConfig.class, ServletContextConfig.class
})
//@ImportResource("classpath:hibernate.cfg.xml")
public class MonAnTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void findMonAns() {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
//        CriteriaQuery<MonAn> searchQuery = cb.createQuery(MonAn.class);
//        Root<MonAn> searchRoot = searchQuery.from(MonAn.class);
//        searchQuery.select(searchRoot);
//        searchQuery.where(getCommonWhereCondition(cb, username, searchRoot, fromDate, toDate, fromTime, toTime));

//        List<Order> orderList = new ArrayList();
//        orderList.add(cb.desc(searchRoot.get("date")));
//        orderList.add(cb.asc(searchRoot.get("time")));
//        searchQuery.orderBy(orderList);

//        TypedQuery<MonAn> filterQuery = em.createQuery(searchQuery)
//                .setFirstResult(0)
//                .setMaxResults(10);

//       List<MonAn> reuslt = filterQuery.getResultList();
//       ;

//        CriteriaQuery<MonAn> all = searchQuery.select(searchRoot);
//        TypedQuery<MonAn> allQuery = em.createQuery(all);
//        List<MonAn> reuslt =   allQuery.getResultList();
//        System.out.println("----" + reuslt.size());

        CriteriaQuery<Meal> searchQueryMeal = cb.createQuery(Meal.class);
        Root<Meal> searchRootMealt = searchQueryMeal.from(Meal.class);
        CriteriaQuery<Meal> allMeal = searchQueryMeal.select(searchRootMealt);
        TypedQuery<Meal> allQueryMeal = em.createQuery(allMeal);
        List<Meal> reusltMeal =   allQueryMeal.getResultList();
        System.out.println("----" + reusltMeal.size());
    }
}
